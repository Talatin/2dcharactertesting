﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CamSizeWire : MonoBehaviour
{
    public int gridSizeX;
    public int gridSizeY;
    public bool ShowWire;
    private float horizontalOffset;
    private float verticalOffset;
    [SerializeField] private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        verticalOffset = cam.orthographicSize * 2f;
        horizontalOffset = verticalOffset * cam.aspect;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (ShowWire)
        {
            for (int i = -gridSizeX; i <= gridSizeX; i++)
            {
                for (int j = -gridSizeY; j <= gridSizeY; j++)
                {
                    Gizmos.DrawWireCube(new Vector3(i * horizontalOffset, j * verticalOffset), new Vector3(horizontalOffset , verticalOffset));
                }
            }
        }
    }
}
