﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementWebinar : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer spRend;
    private float xAxis;
    private bool grounded;
    private bool walledLeft;
    private bool walledRight;
    private bool useGravity = true;
    private Animator animator;

    [Header("Movement Attributes")]
    [SerializeField] private float speed;
    [SerializeField] private float airSteerStrength;
    [SerializeField] private float jumpPower;
    [SerializeField] private float wallJumpPower;
    [SerializeField] private float lowJumpMultiplier;
    [SerializeField] private float fallMultiplier;
    [SerializeField] private bool canDoubleJump;
    [SerializeField] private bool doubleJumpResetOnWall;

    [Header("Wall Slide")]
    [SerializeField] private float wallSlideSpeed;
    [SerializeField] private float wallSlideStrength;

    [Header("Groundchecks")]
    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private float groundCheckRadius;
    [SerializeField] private LayerMask groundLayer;
    private bool doubleJump;
    public bool DoubleJump   // property
    {
        get { return doubleJump; }   // get method
        set { doubleJump = value; }  // set method
    }


    [Header("Wallchecks")]
    [SerializeField] private Transform wallCheckLeftTransform;
    [SerializeField] private Transform wallCheckRightTransform;
    [SerializeField] private float wallCheckRadius;
    [SerializeField] private LayerMask wallLayer;

    [Header("Gizmos")]
    public bool ShowGizmos;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spRend = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        xAxis = Sinput.GetAxis("Horizontal");
        if (Sinput.GetButtonDown("Jump"))
        {
            Jump();
        }
        UpdateAnimator();
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheckTransform.position, groundCheckRadius, groundLayer);
        walledLeft = Physics2D.OverlapCircle(wallCheckLeftTransform.position, wallCheckRadius, wallLayer);
        walledRight = Physics2D.OverlapCircle(wallCheckRightTransform.position, wallCheckRadius, wallLayer);

        GravityManipulation();
        DoubleJumpReset();
        WallSlide();
        Run();
    }

    private void Run()
    {
        if (grounded)
        {
            rb.velocity = new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y);
        }
        else
        {
            rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), airSteerStrength * Time.fixedDeltaTime);
        }

        //flip character
        if (rb.velocity.x < -0.5f)
        {
            spRend.flipX = true;
        }
        else if (rb.velocity.x > 0.5f)
        {
            spRend.flipX = false;
        }
    }

    private void Jump()
    {
        if (walledLeft)
        {
            Vector2 temp = new Vector2(0.75f, 1).normalized;
            rb.velocity = new Vector2(0, Mathf.Min(rb.velocity.y / 2, 1));
            rb.AddForce(temp * wallJumpPower, ForceMode2D.Impulse);
        }
        else if (walledRight)
        {
            Vector2 temp = new Vector2(-0.75f, 1).normalized;
            //Setting velocity.y to  half so the character slightly struggles against gravity.
            rb.velocity = new Vector2(0, Mathf.Min(rb.velocity.y / 2, 1));
            rb.AddForce(temp * wallJumpPower, ForceMode2D.Impulse);
        }

        else if (grounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector2(0, 1) * jumpPower, ForceMode2D.Impulse);
        }
        else if (doubleJump && (!walledLeft && !walledRight) && canDoubleJump)
        {
            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y / 5);
            }
            rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            rb.gravityScale = 1;
            doubleJump = false;
            animator.SetBool("SummerSault", true);
        }
    }

    private void DoubleJumpReset()
    {
        if (doubleJumpResetOnWall)
        {
            if (grounded || walledLeft || walledRight)
            {
                doubleJump = true;
            }
        }
        else if (grounded)
        {
            doubleJump = true;
        }
        if (rb.velocity.y < 0 && canDoubleJump)
        {
            animator.SetBool("SummerSault", false);
        }
    }
    private void WallSlide()
    {
        if ((walledRight && xAxis >= 0 || walledLeft && xAxis <= 0) && rb.velocity.y < 0.1)
        {
            rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(rb.velocity.x, -wallSlideSpeed), wallSlideStrength * Time.fixedDeltaTime);
            rb.gravityScale = 0;
            useGravity = false;
        }
        else
        {
            useGravity = true;
        }
    }
    private void GravityManipulation()
    {
        if (useGravity)
        {
            if (rb.velocity.y < 0)
            {
                rb.gravityScale = fallMultiplier;
            }
            if (rb.velocity.y > 0 && !Sinput.GetButton("Jump"))
            {
                rb.gravityScale = lowJumpMultiplier;
            }
            else if (rb.gravityScale >= 0)
            {
                rb.gravityScale = 1;
            }
        }
    }

    private void UpdateAnimator()
    {
        animator.SetFloat("Horizontal", xAxis);
        animator.SetFloat("RunAnimSpeed", Mathf.Max(Mathf.Abs(xAxis), 0.5f));
        animator.SetFloat("Vertical", rb.velocity.y);
        animator.SetBool("WallContact", (walledLeft || walledRight) && !grounded);
        animator.SetBool("Grounded", grounded);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(groundCheckTransform.position, groundCheckRadius);
    }


}