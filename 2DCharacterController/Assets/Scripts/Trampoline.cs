﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    [SerializeField] private float power;
    private SpriteRenderer spRend;
    [SerializeField] private Color Regular;
    [SerializeField] private Color Bounced;
    [SerializeField] private float lerpTimer;
    private float currentLerpTime;

    private void Start()
    {
        spRend = GetComponent<SpriteRenderer>();
        currentLerpTime = lerpTimer;
    }
    private void Update()
    {
        currentLerpTime += Time.deltaTime;
        float perc = currentLerpTime / lerpTimer;
        spRend.color = Color.Lerp(Bounced, Regular, perc);

        if (currentLerpTime >= lerpTimer)
        {
            currentLerpTime = lerpTimer;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {

            if (Vector3.Dot((collision.transform.position - transform.position).normalized, transform.up) > 0f)
            {
                Rigidbody2D temp = collision.gameObject.GetComponent<Rigidbody2D>();
                //temp.velocity = temp.velocity / 2;
                temp.AddForce(transform.up * power, ForceMode2D.Impulse);
                collision.gameObject.GetComponent<CharacterMovement>().DoubleJump = true;
                setLerpTime();
            }

        }
    }

    private void setLerpTime()
    {
        currentLerpTime = 0;
    }
}
