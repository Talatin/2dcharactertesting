﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCombat : MonoBehaviour
{


    private Animator animator;
    private Rigidbody2D rb;
    private SpriteRenderer spRend;

    [Header("GroundAttack")]
    [SerializeField] float AttackOnePower;
    [SerializeField] float AttackTwoPower;
    [SerializeField] float AttackThreePower;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private Transform attackSideSwitch;
    [SerializeField] private Transform attackOnePos;
    [SerializeField] private Transform attackTwoPos;
    [SerializeField] private Transform attackThreePos;
    [SerializeField] private float attackOneRadius;
    [SerializeField] private float attackTwoRadius;
    [SerializeField] private float attackThreeRadius;

    [Header("AirAttack")]
    [SerializeField] float airAttackOnePower;
    [SerializeField] float airAttackTwoPower;
    [SerializeField] float airAttackDropDownPower;
    [SerializeField] float airAttackThreePower;
    [SerializeField] private Transform airAttackOnePos;
    [SerializeField] private Transform airAttackTwoPos;
    [SerializeField] private Transform airAttackDropDownPos;
    [SerializeField] private Transform airAttackThreePos;
    [SerializeField] private float airAttackOneRadius;
    [SerializeField] private float airAttackTwoRadius;
    [SerializeField] private float airAttackDropDownRadius;
    [SerializeField] private float airAttackThreeRadius;

    private Collider2D[] enemiesHit;
    private List<Collider2D> enemies = new List<Collider2D>();
    [Header("Misc")]
    [Space(10)]
    public bool drawGroundGizmos;
    public bool drawAirGizmos;
    public bool SingleClickToAttack;
    public bool attacking;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        spRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (SingleClickToAttack)
        {
            if (Sinput.GetButtonDown("Fire1"))
            {
                attacking = true;
                animator.SetBool("Attack", attacking);
                animator.SetTrigger("Strike");
            }
        }
        else
        {
            if (Sinput.GetButton("Fire1"))
            {
                attacking = true;
                animator.SetBool("Attack", attacking);
                animator.SetTrigger("Strike");
            }
        }


        SideSwitch();
    }

    private void SideSwitch()
    {
        attackSideSwitch.localScale = spRend.flipX ? new Vector3(-1, 1, 1) : Vector3.one;
    }

    public void SetAttackFalse()
    {
        attacking = false;
        animator.SetBool("Attack", attacking);
    }

    public void AttackOne()
    {
        enemiesHit = Physics2D.OverlapCircleAll(attackOnePos.position, attackOneRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(Vector2.up, AttackOnePower, false);
    }

    public void AttackTwo()
    {
        enemiesHit = Physics2D.OverlapCircleAll(attackTwoPos.position, attackTwoRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(Vector2.up, AttackTwoPower, true);

    }

    public void AttackThree()
    {
        enemiesHit = Physics2D.OverlapCircleAll(attackThreePos.position, attackThreeRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }

        enemiesHit = Physics2D.OverlapCircleAll(attackOnePos.position, attackOneRadius, enemyLayer);
        for (int x = 0; x < enemiesHit.Length; x++)
        {
            enemies.Add(enemiesHit[x]);
        }

        DamageEnemies(Vector2.up, AttackThreePower, true);

    }
    public void AirAttackOne()
    {
        enemiesHit = Physics2D.OverlapCircleAll(airAttackOnePos.position, airAttackOneRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(rb.velocity , airAttackTwoPower, false);
        //attackSideSwitch.transform.localScale.x < 0 ? Vector2.left : Vector2.right
    }
    public void AirAttackTwo()
    {
        enemiesHit = Physics2D.OverlapCircleAll(airAttackOnePos.position, airAttackTwoRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(Vector2.up, airAttackTwoPower, false);
    }

    public void AirAttackDownFall()
    {
        enemiesHit = Physics2D.OverlapCircleAll(airAttackDropDownPos.position, airAttackDropDownRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(Vector2.down, airAttackDropDownPower, false);
    }

    public void AirAttackThree()
    {
        enemiesHit = Physics2D.OverlapCircleAll(airAttackThreePos.position, airAttackThreeRadius, enemyLayer);
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            enemies.Add(enemiesHit[i]);
        }
        DamageEnemies(Vector2.up, airAttackThreePower, true);

    }

    private void DamageEnemies(Vector2 knockBackDir, float knockBackStrength, bool knockAway)
    {
        Rigidbody2D[] tempRbs = new Rigidbody2D[enemies.Count];
        EnemyBehavior[] tempBehaves = new EnemyBehavior[enemies.Count];
        for (int x = 0; x < enemies.Count; x++)
        {
            tempRbs[x] = enemies[x].GetComponent<Rigidbody2D>();
            tempBehaves[x] = enemies[x].GetComponent<EnemyBehavior>();
        }

        if (knockAway)
        {
            for (int x = 0; x < enemies.Count; x++)
            {
                Vector2 temp = (enemies[x].transform.position - (transform.position + Vector3.down * 1.5f)).normalized;
                tempRbs[x].velocity = Vector2.zero;
                tempRbs[x].AddForce(temp * knockBackStrength, ForceMode2D.Impulse);
                tempBehaves[x].setLerpTime();
            }
        }
        else
        {
            for (int x = 0; x < enemies.Count; x++)
            {
                tempRbs[x].velocity = Vector2.zero;
                tempRbs[x].AddForce(knockBackDir * knockBackStrength, ForceMode2D.Impulse);
                tempBehaves[x].setLerpTime();
            }
        }

        enemies.Clear();
    }

    public void DropDown()
    {
        rb.velocity = new Vector2(rb.velocity.x, -10);
    }
    public void MiniAirJump(float jumpPower)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
    }



    private void OnDrawGizmos()
    {
        if (drawGroundGizmos)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(attackOnePos.position, attackOneRadius);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(attackTwoPos.position, attackTwoRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(attackThreePos.position, attackThreeRadius);

        }

        if (drawAirGizmos)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(attackOnePos.position, airAttackOneRadius);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(airAttackTwoPos.position, airAttackTwoRadius);
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(airAttackDropDownPos.position, airAttackDropDownRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(airAttackThreePos.position, airAttackThreeRadius);
        }
    }
}
