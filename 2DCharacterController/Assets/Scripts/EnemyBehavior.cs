﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer spRend;
    [SerializeField] private Color Healthy;
    [SerializeField] private Color Hurt;
    [SerializeField] private float lerpTimer;
    private float currentLerpTime;
    // Start is called before the first frame update
    void Start()
    {
        spRend = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        currentLerpTime = lerpTimer;
    }

    // Update is called once per frame
    void Update()
    {
        currentLerpTime += Time.deltaTime;
        float perc = currentLerpTime / lerpTimer;
        spRend.color = Color.Lerp(Hurt, Healthy, perc);

        if (currentLerpTime >= lerpTimer)
        {
            currentLerpTime = lerpTimer;
        }
    }

    public void setLerpTime()
    {
        currentLerpTime = 0;
    }
}
