﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    private float xAxis;
    private float yAxis;
    private bool grounded;
    private bool walledLeft;
    private bool walledRight;
    public bool useGravity = true;
    private bool movementDisabled;
    private Animator animator;
    private CharacterCombat charCombat;

    [Header("Movement Attributes")]
    [Range(100f, 500f)]
    [SerializeField] private float speed = 250;
    [Range(0.1f, 10f)]
    [SerializeField] private float airSteerStrength = 3;
    [SerializeField] private float jumpPower = 8;
    [SerializeField] private float wallJumpPower = 10;
    [SerializeField] private float lowJumpMultiplier = 2;
    [SerializeField] private float fallMultiplier = 1.5f;
    [SerializeField] private bool canDoubleJump;
    [SerializeField] private bool doubleJumpResetOnWall;

    [Header("Wall Slide")]
    [SerializeField] private float wallSlideSpeed;
    [Range(0.1f, 10f)]
    [SerializeField] private float wallSlideStrength;

    [Header("GroundChecks")]
    [Space(15)]
    [SerializeField] private Transform groundCheckPos;
    [SerializeField] private float groundCheckRadius = 0.15f;
    [SerializeField] private LayerMask groundLayer;
    private bool doubleJump;
    public bool DoubleJump   // property
    {
        get { return doubleJump; }   // get method
        set { doubleJump = value; }  // set method
    }
    [Header("WallChecks")]
    [Space(15)]
    [SerializeField] private Transform wallCheckLeftPos;
    [SerializeField] private Transform wallCheckRightPos;
    [SerializeField] private float wallCheckRadius = 0.05f;
    [SerializeField] private LayerMask wallLayer;

    [Header("Gizmos")]
    [SerializeField] private bool ShowGizmos;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        charCombat = GetComponent<CharacterCombat>();
    }

    private void Update()
    {
        xAxis = Sinput.GetAxis("Horizontal");
        yAxis = Sinput.GetAxis("Vertical");

        UpdateAnimator();

        if (Sinput.GetButtonDown("Jump"))
        {
            Jump();
        }
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheckPos.position, groundCheckRadius, groundLayer);
        walledLeft = Physics2D.OverlapCircle(wallCheckLeftPos.position, wallCheckRadius, groundLayer);
        walledRight = Physics2D.OverlapCircle(wallCheckRightPos.position, wallCheckRadius, groundLayer);


        GravityManipulation();
        Run();
        WallSlide();
        DoubleJumpReset();
    }

    public void DoubleJumpReset()
    { // Resets double jump when the char is either grounded or grounded/walled 
        if (doubleJumpResetOnWall)
        {
            if (grounded || walledLeft || walledRight)
            {
                doubleJump = true;
            }
        }
        else if (grounded)
        {
            doubleJump = true;
        }
        if (rb.velocity.y < 0 && canDoubleJump)
        {
            animator.SetBool("SummerSault", false);
        }
    }

    private void GravityManipulation()
    { // Set Charactergravity according to current y velocity and jump input
        if (useGravity)
        {
            if (rb.velocity.y < 0)
            {
                rb.gravityScale = fallMultiplier;
            }
            if (rb.velocity.y > 0 && !Sinput.GetButton("Jump"))
            {
                rb.gravityScale = lowJumpMultiplier;
            }
            else if (rb.velocity.y >= 0)
            {
                rb.gravityScale = 1;
            }
        }
    }

    private void WallSlide()
    {
        if ((walledRight && xAxis >= 0 || walledLeft && xAxis <= 0) && rb.velocity.y < 0.1)
        {
            rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(rb.velocity.x, -wallSlideSpeed), Time.fixedDeltaTime * wallSlideStrength);
            rb.gravityScale = 0;
            useGravity = false;
        }
        else
        {
            useGravity = true;
        }
    }

    private void Run()
    {
        if (grounded) //Ground Movement = Snappy and fast
        {
            rb.velocity = new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y);
        }
        else //Air movement = Takes more time to reach same speed or stop | Change airSteerStrength to adjust the effect
        {
            rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), airSteerStrength * Time.fixedDeltaTime);
        }

        //Flip character to face movement direction
        if (rb.velocity.x < -1)
        {
            spriteRenderer.flipX = true;
        }
        else if (rb.velocity.x > 1)
        {
            spriteRenderer.flipX = false;
        }
    }

    private void Jump()
    {
        if (walledLeft)
        {
            Vector2 temp = new Vector2(0.75f, 1).normalized;
            //Setting velocity.y to  half so the character slightly struggles against gravity.
            rb.velocity = new Vector2(0, Mathf.Min(rb.velocity.y / 2, 1));
            rb.AddForce(temp * wallJumpPower, ForceMode2D.Impulse);
        }
        else if (walledRight)
        {
            Vector2 temp = new Vector2(-0.75f, 1).normalized;
            //Setting velocity.y to  half so the character slightly struggles against gravity.
            rb.velocity = new Vector2(0, Mathf.Min(Mathf.Max(rb.velocity.y / 2, -4), 1));
            rb.AddForce(temp * wallJumpPower, ForceMode2D.Impulse);
        }
        else if (grounded)
        {
            //Setting velocity.y to 0 so the character doesnt struggle against gravity.
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        }
        else if (doubleJump && (!walledLeft && !walledRight) && canDoubleJump)
        {
            //Setting velocity.y to 0 so the character doesnt struggle against gravity.
            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y / 5);
            }
            rb.gravityScale = 1;
            rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            doubleJump = false;
            animator.SetBool("SummerSault", true);
        }
    }

    private void UpdateAnimator()
    {
        animator.SetFloat("Horizontal", xAxis);
        animator.SetFloat("RunAnimSpeed", Mathf.Max(Mathf.Abs(xAxis), 0.5f));
        animator.SetFloat("Vertical", rb.velocity.y);
        animator.SetBool("WallContact", (walledLeft || walledRight) && !grounded);
        animator.SetBool("Grounded", grounded);
        animator.SetFloat("yAxis", yAxis);
    }

    private void OnDrawGizmos()
    {
        if (ShowGizmos)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(groundCheckPos.position, groundCheckRadius);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(wallCheckRightPos.position, wallCheckRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(wallCheckLeftPos.position, wallCheckRadius);
        }
    }
}
