﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamBehavior : MonoBehaviour
{
    private float horizontalOffset;
    private float verticalOffset;
    [SerializeField] private GameObject Player;
    [SerializeField] private float speed;
    Vector3 Target;
    Vector3 Current;
    Rigidbody2D[] _rb2Ds;
    Vector3[] _rb2DsVelocitys;
    private bool traveling;
    [SerializeField] private bool showGizmos;
    private Camera cam;
    // Use this for initialization
    void Start()
    {
        verticalOffset = GetComponent<Camera>().orthographicSize * 2f;
        horizontalOffset = verticalOffset * GetComponent<Camera>().aspect;
        horizontalOffset /= 2;
        verticalOffset /= 2;
        Target = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        Target = new Vector3(Target.x, Target.y, -10);
        Current = transform.position;
        if (Vector3.Distance(Current, Target) > 0.1)
        {
            transform.position = Vector3.MoveTowards(Current, Target, speed * Time.deltaTime);
        }
    }

    public void MoveNextRoom(Vector2 position)
    {
        Target = position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!traveling)
        {
            if (Player.transform.position.x >= transform.position.x + horizontalOffset)
            {
                Target = new Vector3(horizontalOffset * 2, 0, 0) + transform.position;
                StartCoroutine(FreezeGame());

            }
            if (Player.transform.position.x <= transform.position.x - horizontalOffset)
            {
                Target = new Vector3(-horizontalOffset * 2, 0, 0) + transform.position;
                StartCoroutine(FreezeGame());

            }
            if (Player.transform.position.y >= transform.position.y + verticalOffset)
            {
                Target = new Vector3(0, verticalOffset * 2, 0) + transform.position;
                StartCoroutine(FreezeGame());

            }
            if (Player.transform.position.y <= transform.position.y - verticalOffset)
            {
                Target = new Vector3(0, -verticalOffset * 2, 0) + transform.position;
                StartCoroutine(FreezeGame());
            }
        }
    }

    IEnumerator FreezeGame()
    {
        traveling = true;
        //Player.GetComponent<PlayerMovement>().StunMeDaddy(0.75f);
        Player.GetComponent<Animator>().speed = 0;

        _rb2Ds = Object.FindObjectsOfType<Rigidbody2D>();
        _rb2DsVelocitys = new Vector3[_rb2Ds.Length];
        for (int i = 0; i < _rb2Ds.Length; i++)
        {

            _rb2DsVelocitys[i] = _rb2Ds[i].velocity;
            _rb2Ds[i].bodyType = RigidbodyType2D.Static;

        }
        yield return new WaitForSeconds(0.75f);
        for (int i = 0; i < _rb2Ds.Length; i++)
        {
            _rb2Ds[i].bodyType = RigidbodyType2D.Dynamic;
            _rb2Ds[i].velocity = _rb2DsVelocitys[i];
        }
        Player.GetComponent<Animator>().speed = 1;
        traveling = false;
    }

}
